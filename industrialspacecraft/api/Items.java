package industrialspacecraft.api;

import net.minecraft.item.ItemStack;

public class Items {
	public static ItemStack getItem(String name) throws IllegalArgumentException, SecurityException, IllegalAccessException, NoSuchFieldException, ClassNotFoundException {
		Class<?> ISCItems = Class.forName("industrialspacecraft.items.ModItems");
		Object ret = ISCItems.getField(name).get(null);
		if (ret instanceof ItemStack) {
			return (ItemStack) ret;
		} else {
			return null;
		}
	}
}
