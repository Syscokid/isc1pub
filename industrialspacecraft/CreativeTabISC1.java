package industrialspacecraft;

import industrialspacecraft.lib.ItemIds;
import net.minecraft.creativetab.CreativeTabs;
import cpw.mods.fml.common.Side;
import cpw.mods.fml.common.asm.SideOnly;

public class CreativeTabISC1 extends CreativeTabs {

	public CreativeTabISC1(int par1, String par2Str) {
		super(par1, par2Str);
	}

	@SideOnly(Side.CLIENT)

    /**
     * the itemID for the item to be displayed on the tab
     */
    public int getTabIconItemIndex() {
        return ItemIds.ItemGPSid;
    }

}
