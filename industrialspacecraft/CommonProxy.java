package industrialspacecraft;

public class CommonProxy {
	public static String BLOCK_PNG = "/industrialspacecraft/art/block.png";
	public static String ITEM_PNG = "/industrialspacecraft/art/item.png";
	// Client stuff
	public void registerRenderers () {
		// Nothing here as this is the server side proxy
	}
}
