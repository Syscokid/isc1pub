package industrialspacecraft;

import industrialspacecraft.blocks.ModBlocks;

import java.util.logging.Logger;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;

@Mod(modid = "ISC1", name = "ISC1", version = "alpha2", modExclusionList = "+IC2")
@NetworkMod(clientSideRequired = true, serverSideRequired = true)

public class Main {
	@Instance("ISC1")
	public static Main instance;

	@SidedProxy(clientSide="industrialspacecraft.client.ClientProxy",
			serverSide="industrialspacecraft.CommonProxy")
	public static CommonProxy proxy;
	@PreInit
	public void preInit(FMLPreInitializationEvent event) {
		// Begin Configuration File Stuff
		// Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		// config.load();
		// int ConstructorID = config.getBlock("SpacecraftConstructor.id", 4037).getInt();
		// config.save();
		// End Configuration File Stuff
		Logger iscLog = Logger.getLogger("ISC1");
		iscLog.setParent(FMLLog.getLogger());
		iscLog.info("[ISC1] Starting ISC1 version " + industrialspacecraft.api.ISCApi.getModVersion());
	}

	@Init
	 public void load(FMLInitializationEvent event) {
		ModBlocks.init();
		//* Disabled due to incompleteness *//
		// EntityRegistry.registerModEntity(EntitySpaceCraftTier1.class, "SpaceCraft Tier 1", 1, this, 80, 3, true);
		proxy.registerRenderers();
	}
	@PostInit
	public static void postInit(FMLPostInitializationEvent event) {
		//Stub
	}
}
