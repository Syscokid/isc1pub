package industrialspacecraft.entity;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntitySpaceCraftTier1 extends Entity {
    public EntitySpaceCraftTier1(World par1World)
    {
        super(par1World);
        this.preventEntitySpawning = true;
        this.setSize(1.5F, 0.6F);
        this.yOffset = this.height / 2.0F;
    }
    protected void entityInit()
    {
        this.dataWatcher.addObject(17, new Integer(0));
        this.dataWatcher.addObject(18, new Integer(1));
        this.dataWatcher.addObject(19, new Integer(0));
    }
    public boolean canBePushed()
    {
        return true;
    }
    protected void writeEntityToNBT(NBTTagCompound par1NBTTagCompound) {}
    protected void readEntityFromNBT(NBTTagCompound par1NBTTagCompound) {}
}
