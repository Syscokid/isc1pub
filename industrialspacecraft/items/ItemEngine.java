package industrialspacecraft.items;

import net.minecraft.item.Item;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class ItemEngine extends Item {
	public ItemEngine(int id, int maxStackSize, CreativeTabs tab, int texture, String name) {
		super(id);
		setMaxStackSize(maxStackSize);
		setCreativeTab(tab);
		setIconIndex(texture);
		setItemName(name);
	}
}
