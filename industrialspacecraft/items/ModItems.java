package industrialspacecraft.items;

import industrialspacecraft.lib.ItemIds;
import industrialspacecraft.lib.Strings;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ModItems {
	public static Item GPS = new ItemGPS(ItemIds.ItemGPSid, 1, CreativeTabs.tabTools, 0 * 16 + 0, Strings.ITEM_GPS_NAME);
	public static Item FuelTank = new ItemFuelTank(ItemIds.ItemFuelTankid, 64, CreativeTabs.tabMaterials, 0 * 16 + 0, Strings.ITEM_FUELTANK_NAME);
	public static Item Engine = new ItemEngine(ItemIds.ItemEngineid, 64, CreativeTabs.tabMaterials, 0 * 16 + 0, Strings.ITEM_ENGINE_NAME);
	public static Item Cockpit = new ItemCockpit(ItemIds.ItemEngineid, 64, CreativeTabs.tabMaterials, 0 * 16 + 0, Strings.ITEM_COCKPIT_NAME);
}
