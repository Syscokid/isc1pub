package industrialspacecraft.client;

import net.minecraftforge.client.MinecraftForgeClient;
import industrialspacecraft.CommonProxy;

public class ClientProxy extends CommonProxy {
	public void registerRenderers () {
		MinecraftForgeClient.preloadTexture(BLOCK_PNG);
		MinecraftForgeClient.preloadTexture(ITEM_PNG);
	}
}
