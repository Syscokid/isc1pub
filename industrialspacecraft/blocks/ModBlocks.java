package industrialspacecraft.blocks;

import industrialspacecraft.lib.BlockIds;
import net.minecraft.block.Block;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.common.registry.GameRegistry;

public class ModBlocks {
	public static Block Constructor;

	public static void init() {
		Constructor = new BlockConstructor(BlockIds.Constructor, 0  * 16 + 0);
		GameRegistry.registerBlock(Constructor);
		MinecraftForge.setBlockHarvestLevel(Constructor, "pickaxe", 2);
	}
}
