package industrialspacecraft.blocks;

import industrialspacecraft.CommonProxy;
import industrialspacecraft.lib.Strings;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class BlockConstructor extends Block {
	ItemStack dropOnRightClick = new ItemStack(Item.stick, 1);
	public BlockConstructor(int id, int texture) {
	        super(id, texture, Material.rock);
        	this.blockHardness = 1.0F;
        	this.setBlockName(Strings.Constructor_Name);
        	this.setCreativeTab(CreativeTabs.tabBlock);
	}
	 public boolean onItemRightClick(ItemStack itemStack, World world, EntityPlayer player) {
		player.inventory.addItemStackToInventory(dropOnRightClick);
		return true;
	 }	
	@Override
	public String getTextureFile () {
		return CommonProxy.BLOCK_PNG;
	}
}
