package industrialspacecraft.recipe;

import ic2.api.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
public class Recipe {
	//* Define ItemStacks *//
	public ItemStack ironIngot = new ItemStack(Item.ingotIron, 1);
	public ItemStack blazeRod = new ItemStack(Item.blazeRod, 1);
	public ItemStack enderPearl = new ItemStack(Item.enderPearl, 1);
	public ItemStack GPS = new ItemStack(industrialspacecraft.items.ModItems.GPS, 1);
	public ItemStack refinedIronIngot = Items.getItem("refinedIronIngot");
	public ItemStack uraniumIngot = Items.getItem("uraniumIngot");
	public ItemStack coal = new ItemStack(Item.coal, 1);
	public ItemStack machineBlock = Items.getItem("machine");
	public ItemStack teleporterBlock = Items.getItem("telporter");
	public ItemStack reinforcedGlass = Items.getItem("reinforcedGlass");
	public ItemStack advancedMachineBlock = Items.getItem("advancedMachine");
	public ItemStack lavaCell = Items.getItem("lavaCell");
	public ItemStack electricCircut = Items.getItem("electricCircut");
	public ItemStack carbonMesh = Items.getItem("carbonMesh");
	public ItemStack carbonPlate = Items.getItem("carbonPlate");
	public ItemStack ovScanner = Items.getItem("ovScanner");
	public ItemStack freqTrans = Items.getItem("frequencyTransmitter");
	public ItemStack tin = Items.getItem("tinIngot");
	public ItemStack uraniumCell = Items.getItem("uraniumCell");
	public ItemStack hydratedCoalCell = Items.getItem("hydratedCoalCell");
	public ItemStack euReader = Items.getItem("wrench");
	public ItemStack mvTransformer = Items.getItem("mvTransformer");
	public ItemStack glowstoneDust = new ItemStack(net.minecraft.item.Item.lightStoneDust, 1);
	public ItemStack redstone = new ItemStack(net.minecraft.item.Item.redstone, 1);
	public ItemStack glassPane = new ItemStack(net.minecraft.block.Block.thinGlass, 1);
	public ItemStack FuelTank = new ItemStack(industrialspacecraft.items.ModItems.FuelTank, 1);
	public ItemStack Engine = new ItemStack(industrialspacecraft.items.ModItems.Engine, 1);
	public ItemStack Cockpit = new ItemStack(industrialspacecraft.items.ModItems.Cockpit, 1);
}