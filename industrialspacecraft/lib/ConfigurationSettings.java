package industrialspacecraft.lib;

/**
 * ConfigurationSettings
 * 
 * Stores the various configuration settings read in from configuration files
 * 
 * @author pahimar
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class ConfigurationSettings {
	public static int ConstructorID = BlockIds.Constructor_Default;
}
