package industrialspacecraft.lib;

public class Strings {

	//* Strings are very important! *//
	public static final String Constructor_Name = "SpacecraftConstructor";
	public static final String ITEM_GPS_NAME = "GPS";
	public static final String ITEM_FUELTANK_NAME = "FuelTank";
	public static final String ITEM_ENGINE_NAME = "Engine";
	public static final String ITEM_COCKPIT_NAME = "Cockpit";
}
