#!/bin/sh
mcforge_url="http://files.minecraftforge.net/minecraftforge-src-latest.zip"
[ -d "build" ] && echo "Setup already completed. If you want to setup again remove build directory then re-run setup" && exit 1
[ ! -d "build" ] && mkdir -p build
echo '* Downloading Forge and Unziping *'
cd build
wget -Omcforge.zip $mcforge_url >/dev/null
unzip mcforge.zip >/dev/null
rm mcforge.zip
echo '* Installing Forge *'
cd forge
sh install.sh
cd ../..
echo '* Build Enviroment Setup complete *'
